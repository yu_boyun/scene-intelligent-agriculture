#  智能浇水器

## 介绍

基于 Arduino/ESP32 的智能浇水器，通过翼辉官方 sddc_demo 修改而来。

## 使用说明

1. 管脚使用 IO12 控制浇水

   ​                A0  检测浇水器是否休眠

   ​                IO 5 监控浇水状态

2. 控制板和浇水器分别供5V电

3. 查询报文 json 格式（基于 EdgerOS 的测试工具的使用，请参考该[文档](https://blog.csdn.net/lixiaocheng1983/article/details/119652855?spm=1001.2014.3001.5501)）： 

   ```
   {
     "method": "set",
     "watering": "ON"/"OFF"
   }
   ```

   ```
   {
     "method": "get",
     "obj": ["watering"]
   }
   ```

   

4. 需要搭配 Edgeros 和 SDDC 使用

5. 通过手机控制浇水器浇水

6. 需要引入 libsddc，cjson 这几个库

## 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull **Request**