/*
 * 土壤湿度传感器
 * 命令示例：
{
  "method": "get",
  "obj": ["soil_humidity"]
}
{
  "method": "set",
  "periodic_time": 1000
}
*/
#include "Arduino.h"    
#include <OneButton.h>       
#include <WiFi.h>
#include <SDDC_SDK_lib.h>
#include <cJSON.h>
#include <Wire.h>


#define SDDC_CFG_PORT             680U             // SDDC 协议使用的端口号
#define PIN_INPUT 0                                // 选择 IO0 进行控制
#define ESP_TASK_STACK_SIZE   4096
#define ESP_TASK_PRIO         25

static const int sensor_in = A0;                   // 数据输入引脚

static const char* ssid     = "EOS-Tenda";         // WiFi 名
static const char* password = "1234567890";        // WiFi 密码

const float AirValue = 3069;                       //初始化最大干燥 （传感器在空中的情况）
const float WaterValue = 1400;                     //初始化最大湿度 （传感器放入水中的情况）
int intervals = (AirValue - WaterValue) / 3;

static  int xTicksToDelay = 10000;                 // 周期延时时间
  
OneButton button(PIN_INPUT, true);

// 周期上报函数
static void periodic_sensor_task(void *arg)
{
    while(1)
    {
        // 任务创建之后，设定延时周期
        delay(xTicksToDelay);
    
        report_sensor_state();
        delay(100);
    }  
    // 已停止发送数据
    Serial.printf("Soil humidity data OFF\n");
}


// 主动数据上报函数
static void report_sensor_state()
{  
    int sensorValue = 0;
    cJSON *value;
    cJSON *root;
    char  *msg;
     
    value =  cJSON_CreateArray();
    root = cJSON_CreateObject();
    sddc_return_if_fail(value);
    sddc_return_if_fail(root);
      
    sddc_return_if_fail(value);
      
    // 获取传感器数据
    cJSON_AddItemToArray(value, cJSON_CreateString("soil_humidity"));
    cJSON_AddItemToObject(root, "obj", value);
      
    // 发送数据给 EdgerOS
    msg = cJSON_Print(root);
    sddc_printf("主动上报: %s\n",msg);
    object_report(root);
      
    cJSON_Delete(value);
    cJSON_free(msg);
}

/* 
 *  设置周期等待时间
 */
sddc_bool_t periodic_time_set(const uint64_t value)
{
    sddc_printf("修改上报周期!\n");
    xTicksToDelay = value;
    return SDDC_TRUE;
}
/* 
 *  单次获取数据.
 */
sddc_bool_t single_get_sensor(char *objvalue, int value_len)
{
    float value = 100 - (((analogRead(sensor_in))-WaterValue)/(AirValue - WaterValue))*100;
    if(value > 100)
    {
        value = 100;
    } else if(value < 0)
    {
        value = 0;
    }
    snprintf(objvalue, value_len, "%f", value);
    return SDDC_TRUE;
}
 
/* 
 *  数字量设备对象函数与处理方法注册
 */
NUM_DEV_REGINFO num_dev[] = {
        {"periodic_time",periodic_time_set},
};

/*
 *  显示设备对象函数与处理方法注册
 */
DIS_DEV_REGINFO dis_dev[] = {
};

/*
 * IO设备对象设置函数与处理方法注册
 */
IO_DEV_REGINFO io_dev[] = {
};

/*
 *  系统对象状态获取注册
 */
DEV_STATE_GET  dev_state_get_reg[] = {
        {"soil_humidity",   DEV_NUM_TYPE,  single_get_sensor},
};

/*
 *  当前设备的信息定义
 */
DEV_INFO    dev_info = {
            .name     = "土壤湿度传感器",
            .type     = "device",
            .excl     = SDDC_FALSE,
            .desc     = "ESP-32S + SEN0193",
            .model    = "1",
            .vendor   = "inspiration-desktop",
};

/*
 *   系统注册对象汇聚
 */
SDDC_CONFIG_INFO sys_cfg = {
        .token             = "1234567890",            // 设备密码
        .devinfo           = &dev_info,               
        .io_dev_reg        = io_dev,
        .io_dev_reg_num    = ARRAY_SIZE(io_dev),
        .num_dev_reg       = num_dev,
        .num_dev_reg_num   = ARRAY_SIZE(num_dev),
        .state_get_reg     = dev_state_get_reg,
        .state_get_reg_num = ARRAY_SIZE(dev_state_get_reg),
        .dis_dev_reg       = dis_dev,
        .dis_dev_num       = ARRAY_SIZE(dis_dev),
};


/*
 * IO0 按键检测任务
 */
static void esp_io0_key_task()
{
    WiFi.disconnect();
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(1000);
        Serial.println("WIFI Wait for Smartconfig");
        
        // 设置为Station模式
        WiFi.mode(WIFI_STA);
        
        // 开始智能配网
        WiFi.beginSmartConfig();
        while (1)
        {
            delay(500);
            // 检查智能配网
            if (WiFi.smartConfigDone())
            {
                // 设置自动连接
                WiFi.setAutoConnect(true);  
                break;
            }
        }
    }
}
/*
 * 循环扫描按键
 */
static void esp_tick_task(void *arg)
{
    void *sddc = arg;
    (void)sddc;
    while(1)
    {
        button.tick();
        delay(100);
    }
}

/*
 * 初始化传感器
 */
void sensor_init()
{
    // 创建传感器任务，周期性获取土壤湿度传感器的数据并发送给 EdgerOS
    xTaskCreate(periodic_sensor_task, "periodic_sensor_task", ESP_TASK_STACK_SIZE, NULL, ESP_TASK_PRIO, NULL);
}

void setup() {
    byte mac[6];
    Serial.begin(115200);
    Serial.setDebugOutput(true);
    Serial.println();

    // 初始化传感器
    sensor_init();

    // 清除一下按键状态机的状态
    button.reset();
  
    // 创建按键扫描线程，长按 IO0 按键，松开后ESP32 将会进入 SmartConfig 模式
    sddc_printf("长按按键进入 Smartconfig...\n");
    button.attachLongPressStop(esp_io0_key_task);
    xTaskCreate(esp_tick_task, "button_tick", ESP_TASK_STACK_SIZE, NULL, ESP_TASK_PRIO, NULL);
    
    // 启动 WiFi 并且连接网络
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) 
    {
        delay(500);
        Serial.print(".");
    }
  
    // 获取并打印 IP 地址
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.print("'ip :");
    Serial.print(WiFi.localIP());
    Serial.println("' to connect"); 
  
    // sddc协议初始化
    sddc_lib_main(&sys_cfg);

    // 获取并打印网卡 mac 地址
    WiFi.macAddress(mac);
    sddc_printf("MAC addr: %02x:%02x:%02x:%02x:%02x:%02x\n",
              mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);
    // 使用网卡 mac 地址设置设备唯一标识 UID
    sddc_set_uid(G_sddc, mac);
}

void loop() {
    // 运行 SDDC 协议循环
    while (1) 
    {
        sddc_printf("SDDC running...\n");
        sddc_run(G_sddc);
        sddc_printf("SDDC quit!\n");
    }

    // 销毁 SDDC 协议
    sddc_destroy(G_sddc);
}
