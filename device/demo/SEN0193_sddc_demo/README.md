#  土壤湿度传感器

## 介绍

基于 Arduino/ESP32 的水分传感器，通过翼辉官方 sddc_demo 修改而来。

## 使用说明

1. 管脚使用 A0 控制（SVP/IO36）

2. 需要输入3.3 ~ 5.5 电压

3. 查询报文 json 格式（基于 EdgerOS 的测试工具的使用，请参考该[文档](https://blog.csdn.net/lixiaocheng1983/article/details/119652855?spm=1001.2014.3001.5501)）：

   ```
   {
     "method": "get",
     "obj": ["soil_humidity"]
   }
   {
     "method": "set",
     "periodic_time": 1000
   }
   ```

4. 需要搭配 Edgeros 和 SDDC 使用

5. 获取土壤湿度

6. 需要引入 libsddc，cjson 这几个库

## 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull **Request**