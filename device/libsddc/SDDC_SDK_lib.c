#include "cJSON.h"
#include "SDDC_SDK_lib.h"
#include "sddc.h"
#include "sddc_list.h"

#define DDC_METHOD_VALIDE 0
#define DDC_METHOD_SET    1
#define DDC_METHOD_GET    2
#define DDC_METHOD_REPORT 3

static SDDC_CONFIG_INFO *G_config;
static int              sockfd;
sddc_t           *G_sddc = NULL;
char                    cstate_buf[STATE_GET_BUF_LEN];
/*********************************************************************************************************
** 函数名称: object_IO_Set
** 功能描述: SDDC协议收到消息后，开关设备处理模板
** 输　入  : input        收入的cJSON报文
**           object_name  注册的对象名称
**           do_fun       注册的该对象的处理函数
** 输　出  : 0: 处理失败, 1: 处理成功.
** 全局变量:
** 调用模块: 无
*********************************************************************************************************/
static sddc_bool_t object_IO_Set(cJSON   *input,  const char *object_name, Io_Set do_fun)
{
    cJSON   *Json_object;

    Json_object = cJSON_GetObjectItem(input, object_name);
    if (NULL == Json_object) {
        return SDDC_FALSE;
    }

    if (cJSON_IsString(Json_object)) {
        return do_fun(Json_object->valuestring);
    } else {
        return SDDC_FALSE;
    }

    return SDDC_TRUE;
}

/*********************************************************************************************************
** 函数名称: object_Number_Set
** 功能描述: SDDC协议收到消息后，数字量设备处理模板
** 输　入  : input        收入的cJSON报文
**           object_name  注册的对象名称
**           do_fun       注册的该对象的处理函数
** 输　出  : 0: 处理失败, 1: 处理成功.
** 全局变量:
** 调用模块: 无
*********************************************************************************************************/
static sddc_bool_t object_Number_Set(cJSON   *input,  const char *object_name, Num_Set do_fun)
{
    cJSON   *Json_object;

    Json_object = cJSON_GetObjectItem(input, object_name);
    if (NULL == Json_object) {
        return SDDC_FALSE;
    }

    if (cJSON_IsNumber(Json_object)) {
        return do_fun((uint64_t)Json_object->valuedouble);
    } else {
        return SDDC_FALSE;
    }

    return SDDC_TRUE;
}

/*********************************************************************************************************
** 函数名称: object_Display_Set
** 功能描述: SDDC协议收到消息后，显示类设备处理模板
** 输　入  : input        收入的cJSON报文
**           object_name  注册的对象名称
**           do_fun       注册的该对象的处理函数
** 输　出  : 0: 处理失败, 1: 处理成功.
** 全局变量:
** 调用模块: 无
*********************************************************************************************************/
static sddc_bool_t object_Display_Set(cJSON   *input,  const char *object_name, Dis_Set do_fun)
{
    cJSON   *Json_object;

    Json_object = cJSON_GetObjectItem(input, object_name);
    if (NULL == Json_object) {
        return SDDC_FALSE;
    }

    if (cJSON_IsObject(Json_object)) {
        return do_fun(Json_object);
    } else {
        return SDDC_FALSE;
    }

    return SDDC_TRUE;
}

/*********************************************************************************************************
** 函数名称: object_get
** 功能描述: SDDC协议收到消息后，根据注册内容给客户反馈消息
** 输　入  :    input        收入的cJSON报文
**           object_name  注册的对象名称
**           do_fun       注册的该对象的处理函数
** 输　出  : 0: 处理失败, 1: 处理成功.
** 全局变量:
** 调用模块: 无
*********************************************************************************************************/
static sddc_bool_t object_get(sddc_t *sddc, const uint8_t *uid, cJSON   *input,
                            DEV_STATE_GET *state_get_list, int list_len, sddc_bool_t report_flag)
{
    cJSON   *Json_object;
    cJSON   *root;
    cJSON   *json_data;
    char    *str;
    int     i,j;

    Json_object = cJSON_GetObjectItem(input, "obj");
    if (NULL == Json_object) {
        return SDDC_FALSE;
    }

    if (!cJSON_IsArray(Json_object)) {
        return SDDC_FALSE;
    }

    root = cJSON_CreateObject();
    if (report_flag) {
		cJSON_AddStringToObject(root, "method", "report");
	} else {
		cJSON_AddStringToObject(root, "method", "get");
	}
    json_data = cJSON_CreateObject();
	
    for (i=0; i<cJSON_GetArraySize(Json_object); i++) {
        for (j=0; j<list_len; j++) {
            if (strcmp(cJSON_GetArrayItem(Json_object, i)->valuestring, state_get_list[j].objname) == 0) {
                if (state_get_list[j].type == DEV_IO_TYPE) {
                    if (state_get_list[j].state_fun(cstate_buf, sizeof(cstate_buf)) == SDDC_TRUE) {
                        cJSON_AddStringToObject(json_data, state_get_list[j].objname, cstate_buf);
                    }
                } else if (state_get_list[j].type == DEV_NUM_TYPE ){
                    if (state_get_list[j].state_fun(cstate_buf, sizeof(cstate_buf)) == SDDC_TRUE) {
                        cJSON_AddNumberToObject(json_data, state_get_list[j].objname, atof(cstate_buf));
                    }
                } else if (state_get_list[j].type == DEV_DISPLAY_TYPE) {
                    if (state_get_list[j].state_fun(cstate_buf, sizeof(cstate_buf)) == SDDC_TRUE) {
                        cJSON   *display_obj;
                        display_obj = cJSON_AddObjectToObject(json_data, state_get_list[j].objname);
                        cJSON_AddStringToObject(display_obj, state_get_list[j].objname, cstate_buf);
                    }
                }
            }
        }
    }
    cJSON_AddItemToObject(root, "data", json_data);
    str = cJSON_Print(root);
	sddc_return_value_if_fail(str, NULL);
    printf("object_get str = %s\n", str);
    if (!uid) {
        sddc_broadcast_message(sddc, str, strlen(str), 3, SDDC_FALSE, NULL);
    } else {
        sddc_send_message(sddc, uid, str, strlen(str), 3, SDDC_FALSE, NULL);
    }

    cJSON_free(str);

    cJSON_Delete(root);

    return SDDC_TRUE;
}

/*********************************************************************************************************
** 函数名称: sddc_on_message_lib
** 功能描述: SDDC协议收到消息后的处理回调函数
** 输　入  : sddc          SDDC结构体
**           uid           发送消息的ID
**           message       接收到的报文消息
**           len           报文长度
** 输　出  : 0: 处理失败, 1: 处理成功.
** 全局变量:
** 调用模块: 无
*********************************************************************************************************/
static sddc_bool_t sddc_on_message_lib(sddc_t *sddc, const uint8_t *uid, const char *message, size_t len)
{
    cJSON   *root    = cJSON_Parse(message);
    cJSON   *Json_method;
    uint8_t uimethod =DDC_METHOD_VALIDE;

    Json_method = cJSON_GetObjectItem(root, "method");
    if (NULL == Json_method) {
        return SDDC_FALSE;
    }

    if (cJSON_IsString(Json_method)) {
        if (strcmp(Json_method->valuestring,"set") == 0) {
            uimethod = DDC_METHOD_SET;
        } else if (strcmp(Json_method->valuestring,"get") == 0) {
            uimethod = DDC_METHOD_GET;
        } else {
            return SDDC_FALSE;
        }
    } else {
        return SDDC_FALSE;
    }

    if (uimethod == DDC_METHOD_VALIDE) {
        return SDDC_FALSE;
    }

    if (uimethod == DDC_METHOD_SET) {
        int i;

        /*
         *  数字量、显示量先查询设置，防止开关量是设备的使能
         */
        for (i=0; i<G_config->num_dev_reg_num; i++) {
            object_Number_Set(root, G_config->num_dev_reg[i].objname, G_config->num_dev_reg[i].Num_Fun);
        }

        for (i=0; i<G_config->dis_dev_num; i++) {
            object_Display_Set(root, G_config->dis_dev_reg[i].objname, G_config->dis_dev_reg[i].Dis_Fun);
        }

        for (i=0; i<G_config->io_dev_reg_num; i++) {
            object_IO_Set(root, G_config->io_dev_reg[i].objname, G_config->io_dev_reg[i].IO_Fun);
        }
    } else if (uimethod == DDC_METHOD_GET) {
        object_get(sddc, uid, root, G_config->state_get_reg, G_config->state_get_reg_num, 0);
    } else {
        return SDDC_FALSE;
    }

    return SDDC_TRUE;
}


/*********************************************************************************************************
** 函数名称: sddc_on_message_ack_lib
** 功能描述: SDDC协议收到消息后的处理回调函数
** 输　入  : sddc          SDDC结构体
**           uid           发送目标的ID
**           seqno         应答序号
** 输　出  : 0: 处理失败, 1: 处理成功.
** 全局变量:
** 调用模块: 无
*********************************************************************************************************/
static void sddc_on_message_ack_lib(sddc_t *sddc, const uint8_t *uid, uint16_t seqno)
{
    // 暂不处理
}

/*********************************************************************************************************
** 函数名称: sddc_on_message_lost_lib
** 功能描述: SDDC协议丢失消息后的处理回调函数
** 输　入  : sddc          SDDC结构体
**           uid           发送给目标的ID
**           seqno         丢失报文序号
** 输　出  : 0: 处理失败, 1: 处理成功.
** 全局变量:
** 调用模块: 无
*********************************************************************************************************/
static void sddc_on_message_lost_lib(sddc_t *sddc, const uint8_t *uid, uint16_t seqno)
{
    printf("msg lost seqno is %d\r\n", seqno);
}

/*********************************************************************************************************
** 函数名称: iot_pi_on_edgeros_lost_lib
** 功能描述: SDDC丢失EdgerOS的处理
** 输　入  : sddc          SDDC结构体
**           uid           丢失的目标序号
** 输　出  : 0: 处理失败, 1: 处理成功.
** 全局变量:
** 调用模块: 无
*********************************************************************************************************/
static void sddc_on_edgeros_lost_lib(sddc_t *sddc, const uint8_t *uid)
{
    printf("edgeros lost seqno is %lld\r\n", *(long long*)uid);
}

/*********************************************************************************************************
** 函数名称: sddc_on_update_lib
** 功能描述: SDDC 收到EdgerOS请求，并立即更新反馈结果给EdgerOS
** 输　入  : sddc          SDDC结构体
**           uid           EdgerOS序号
**           update_data   接收到的报文
**           len           接收到的长度
** 输　出  : 0: 处理失败, 1: 处理成功.
** 全局变量:
** 调用模块: 无
*********************************************************************************************************/
static sddc_bool_t sddc_on_update_lib(sddc_t *sddc, const uint8_t *uid, const char *update_data, size_t len)
{
    cJSON *root = cJSON_Parse(update_data);

    if (root) {
        /*
         * Parse here
         */

        char *str = cJSON_Print(root);

        printf("iot_pi_sddc_on_update: %s\n", str);

        cJSON_free(str);

        cJSON_Delete(root);

        return SDDC_TRUE;
    } else {
        return SDDC_FALSE;
    }
}

/*********************************************************************************************************
** 函数名称: sddc_on_invite_lib
** 功能描述: SDDC 收到EdgerOS邀请消息时候的应答
** 输　入  : sddc          SDDC结构体
**           uid           EdgerOS序号
**           update_data   接收到的报文
**           len           接收到的长度
** 输　出  : 0: 处理失败, 1: 处理成功.
** 全局变量:
** 调用模块: 无
*********************************************************************************************************/
static sddc_bool_t sddc_on_invite_lib(sddc_t *sddc, const uint8_t *uid, const char *invite_data, size_t len)
{
    cJSON *root = cJSON_Parse(invite_data);

    if (root) {
        /*
         * Parse here
         */

        char *str = cJSON_Print(root);

        printf("iot_pi_on_invite: %s\n", str);

        cJSON_free(str);

        cJSON_Delete(root);

        return SDDC_TRUE;
    } else {
        return SDDC_FALSE;
    }
}

/*********************************************************************************************************
** 函数名称: sddc_on_invite_end_lib
** 功能描述: SDDC 收到EdgerOS邀请消息结束时候的应答
** 输　入  : sddc          SDDC结构体
**           uid           EdgerOS序号
** 输　出  : 0: 处理失败, 1: 处理成功.
** 全局变量:
** 调用模块: 无
*********************************************************************************************************/
static sddc_bool_t sddc_on_invite_end_lib(sddc_t *sddc, const uint8_t *uid)
{
//    iot_pi_led_state_report(sddc, uid, NULL);

    return SDDC_TRUE;
}

/*********************************************************************************************************
** 函数名称: sddc_report_data_create
** 功能描述: SDDC 收到report消息时候回复的内容
** 输　入  : devinfo         设备信息
** 输　出  : 0: 处理失败, 1: 处理成功.
** 全局变量:
** 调用模块: 无
*********************************************************************************************************/
static char *sddc_report_data_create(DEV_INFO *devinfo)
{
    cJSON *root;
    cJSON *report;
    char  *str;

    root = cJSON_CreateObject();
    cJSON_AddItemToObject(root, "report", report = cJSON_CreateObject());
        cJSON_AddStringToObject(report, "name",    devinfo->name);
        cJSON_AddStringToObject(report, "type",    devinfo->type);
        cJSON_AddBoolToObject(report,   "excl",    devinfo->excl);
        cJSON_AddStringToObject(report, "desc",    devinfo->desc);
        cJSON_AddStringToObject(report, "model",   devinfo->model);
//        cJSON_AddNumberToObject(report, "version", devinfo->version);
        cJSON_AddStringToObject(report, "vendor",  devinfo->vendor);

    /*
     * Add extension here
     */

    str = cJSON_Print(root);
    printf("REPORT DATA: %s\n", str);

    cJSON_Delete(root);

    return str;
}

/*********************************************************************************************************
** 函数名称: sddc_invite_data_create
** 功能描述: SDDC 收到邀请消息时候的回复如下
** 输　入  : sddc          SDDC结构体
**           uid           EdgerOS序号
** 输　出  : 0: 处理失败, 1: 处理成功.
** 全局变量:
** 调用模块: 无
*********************************************************************************************************/
static char *sddc_invite_data_create(DEV_INFO *devinfo)
{
    return sddc_report_data_create(devinfo);
}

/*********************************************************************************************************
** 函数名称: object_report
** 功能描述: 封装给外部用户用的上报接口
** 输　入  : reportlist   上报的内容列表，格式为{obj:"LED1","LED2","TMP"}
** 输　出  : 0: 处理失败, 1: 处理成功.
** 全局变量:
** 调用模块: 无
*********************************************************************************************************/
int object_report(cJSON *reportlist)
{
    if (NULL == G_sddc) {
        return SDDC_FALSE;
    }

    return object_get(G_sddc, NULL, reportlist, G_config->state_get_reg, G_config->state_get_reg_num, 1);
}

/*********************************************************************************************************
** 函数名称: sddc_lib_main
** 功能描述: SDDC SDK 的总入口函数
** 输　入  : sddc          SDDC结构体
**           uid           EdgerOS序号
** 输　出  : 0: 处理失败, 1: 处理成功.
** 全局变量:
** 调用模块: 无
*********************************************************************************************************/
int sddc_lib_main(SDDC_CONFIG_INFO * config)
{
     char                *data;

	 if (config == NULL) {
			return SDDC_FALSE;
		}
			
		G_config = config;
	//#ifdef SDDC_CFG_NET_IMPL
	//	  int ret = ms_net_set_impl(SDDC_CFG_NET_IMPL);
	//	  sddc_return_value_if_fail(ret == MS_ERR_NONE, -1);
	//#endif
	
		/*
		 * Create SDDC
		 */
		G_sddc = sddc_create(SDDC_CFG_PORT);
	
		/*
		 * Set call backs
		 */
		sddc_set_on_message(G_sddc, sddc_on_message_lib);
		sddc_set_on_message_ack(G_sddc, sddc_on_message_ack_lib);
		sddc_set_on_message_lost(G_sddc, sddc_on_message_lost_lib);
		sddc_set_on_edgeros_lost(G_sddc, sddc_on_edgeros_lost_lib);
		sddc_set_on_invite(G_sddc, sddc_on_invite_lib);
		sddc_set_on_invite_end(G_sddc, sddc_on_invite_end_lib);
		sddc_set_on_update(G_sddc, sddc_on_update_lib);
	
		/*
		 * Set token
		 */
#if SDDC_CFG_SECURITY_EN > 0
		sddc_set_token(G_sddc, config->token);
#endif
	
		/*
		 * Set report data
		 */
		data = sddc_report_data_create(config->devinfo);
		sddc_set_report_data(G_sddc, data, strlen(data));
	
		/*
		 * Set invite data
		 */
		data = sddc_invite_data_create(config->devinfo);
		sddc_set_invite_data(G_sddc, data, strlen(data));
		
		return SDDC_TRUE;


}

