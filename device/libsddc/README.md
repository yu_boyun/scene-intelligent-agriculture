# lib_sddc

#### 介绍
移植自官方的 SDDC 库，arduino 默认使用 POSIX 接口，库另外还支持 free-rtos和 ms-rtos


#### 安装教程

1.  将文件夹复制放入 arduino -IDE  安装目录下的 libraries 目录
2.  重启 arduino -IDE 后加载库

#### 使用说明

1.  在 arduino -IDE 中加载库

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

6.  https://gitee.com/gitee-stars/)

