# lib_arduino

#### 介绍
这是 arduino demo 会依赖的个人移植库，


#### 安装教程

1.  将文件夹复制放入 arduino -IDE  安装目录下的 libraries 目录
2.  重启 arduino -IDE 后加载库

#### 使用说明

1.  在 arduino -IDE 中加载库

#### 目录

2.  [cjson](./cjson) 移植的cjson库
3.  [demo](./demo) 智能设备demo程序
3. [libsddc](./libsddc) 移植自翼辉官方的 SDDC 库，添加了自己写的SDK框架

   

代码来源：https://gitee.com/inspiration-desktop/DEV-lib-arduino

