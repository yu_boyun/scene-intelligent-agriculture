const LightKV = require("lightkv");

/**
 * {
 *   humidity_water: {
 *      devids: string[];
 *      settings: {
 *        label: string
 *        points: number[]
 *      }
 *   }
 * }
 */
const sceneDB = new LightKV('scene_db.lkv', 'c+', LightKV.OBJECT);

module.exports = { sceneDB };