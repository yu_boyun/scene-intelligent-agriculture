const SocketIO = require('socket.io');

function initSocketIO(app, option) {
  const opts = typeof option === 'object' ? option : {}
  return SocketIO(app, {
    serveClient: false,
    pingInterval: 10000,
    pingTimeout: 5000,
    cookie: false,
    ...opts
  });
}

module.exports = {
  initSocketIO
}

