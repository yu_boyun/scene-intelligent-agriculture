import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import store from '@/store';
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.baseURL = process.env.NODE_ENV === 'development' ? '/edgerApi' : '/';

axios.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    config.headers = {
      ...config.headers,
      'edger-token': store.state.token || '',
      'edger-srand': store.state.srand || ''
    };
    return config;
  },
  (error: Error) => {
    throw error;
  }
);

axios.interceptors.response.use(
  (response: AxiosResponse) => {
    console.log('[http success]：', response);
    return response.data;
  },
  (error: Error) => {
    console.log('[http failed]：', error);
    throw error;
  }
);

export default axios;
