import { IResponse } from '@/interfaces/common.interface';
import { IDeviceListResp, IInitSceneResp } from '@/interfaces/api.interface';
import httpRequest from '../http';

function getDeviceList(): Promise<IDeviceListResp> {
  return httpRequest.get('/device-list');
}

function initSceneDev(): Promise<IInitSceneResp> {
  return httpRequest.post('/init-scene');
}

function changeSceneDevice(params: { add_id: string; delete_id: string }): Promise<IResponse> {
  return httpRequest.post('/change-scene-device', params);
}

// eslint-disable-next-line
function sendControlMessage(params: { devid: string; data: { [propName: string]: any } }): Promise<IResponse> {
  return httpRequest.post('/control-message', params);
}

function changeSceneSetting(params: {points: number[], label?: string}): Promise<IResponse> {
  return httpRequest.post('/change-scene-setting', params);
}

export default {
  getDeviceList,
  initSceneDev,
  changeSceneDevice,
  sendControlMessage,
  changeSceneSetting
};
