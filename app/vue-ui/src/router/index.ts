import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/Home'),
    redirect: 'water',
    children: [
      {
        path: '/water',
        name: 'water',
        meta: { index: 10 },
        component: () => import('../views/Water')
      }
    ]
  }
];

const router = new VueRouter({
  routes
});

export default router;
