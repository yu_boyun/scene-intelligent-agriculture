import Vue from 'vue';
import './registerServiceWorker';
import router from './router';
import 'normalize.css';
import { edger } from '@edgeros/web-sdk';
import App from './App';
import 'vant/lib/index.less';
import { initSocket } from './services/socket';
edger.avoidKeyboardOverlay();

Vue.config.productionTip = false;

import Api from './services/apis/index';
Vue.prototype.$api = Api;
Vue.prototype.$edger = edger;

import store from '@/store';

edger.token().then(data => {
  if (data) {
    store.commit('update', { token: data.token, srand: data.srand });
    initSocket(data);
  }
});

edger.onAction('token', data => {
  if (data) {
    store.commit('update', { token: data.token, srand: data.srand });
  }
});

edger
  .user()
  .then(result => {
    if (result && result.acoid) {
      store.commit('update', { accountInfo: result });
    } else {
      edger.notify.error('当前登录信息出错，请检查重启！');
    }
  })
  .catch((error: Error) => {
    edger.notify.error('请先登录爱智账号！');
    throw error;
  })
  .finally(() => {
    new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app');
  });
