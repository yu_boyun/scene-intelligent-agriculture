import { Vue, Component } from 'vue-property-decorator';
import style from '@/styles/views/home.module.less';

@Component({})
export default class Home extends Vue {
  public render() {
    return (
      <div class={style['home']}>
        <div class={style['home-container']}>
            <router-view />
        </div>
      </div>
    );
  }
}
