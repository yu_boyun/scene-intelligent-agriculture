import { Component, Prop, Vue, Watch } from 'vue-property-decorator';
import style from '@/styles/components/echarts.module.less';
import echarts, { ECharts } from 'echarts';

@Component({})
export default class Hygrometer extends Vue {
  @Prop({ type: Number }) private value!: number;
  @Prop({ type: Array }) private points!: number[];

  private chartTarget: ECharts | null = null;
  private realValue = 0;

  public mounted() {
    this.chartTarget = echarts.init(document.getElementById('hygrometer') as HTMLDivElement);
    this.drawChart(this.points);
  }

  @Watch('value', { immediate: true })
  public watchValue(v: number) {
    this.realValue = v;
    this.drawChart(this.points);
  }

  @Watch('points')
  public watchPoints(v: number[]) {
    this.drawChart(v);
  }

  public render() {
    return <div id='hygrometer' class={style['hygrometer']}></div>;
  }

  private drawChart(points: number[]) {
    // 基于准备好的dom，初始化echarts实例
    const scaleLineMarks = [];
    let Gradient = [
      {
        offset: 1,
        color: '#46a3ff'
      }
    ];
    if (this.realValue < points[0]) {
      Gradient = [
        {
          offset: 1,
          color: '#ee0a24'
        }
      ];
    }
    let showValue = 0;
    // 刻度使用柱状图模拟，短设置1，长的设置3；构造一个数据
    for (let i = 0, len = 60; i <= len; i += 1) {
      if (i < 10) {
        scaleLineMarks.push('');
      } else if ((i - 10) % 10 === 0) {
        scaleLineMarks.push('-3');
      } else {
        scaleLineMarks.push('-1');
      }
    }
    //中间线的渐变色和文本内容
    // if (this.realValue > points[1]) {
    //   Gradient.push(
    //     {
    //       offset: 0,
    //       color: '#E01F28'
    //     },
    //     {
    //       offset: points[0] / 100,
    //       color: '#E4D225'
    //     },
    //     {
    //       offset: 1,
    //       color: '#93FE94'
    //     }
    //   );
    // } else if (this.realValue >= points[0]) {
    //   Gradient.push(
    //     {
    //       offset: 0,
    //       color: '#E01F28'
    //     },
    //     {
    //       offset: points[0] / 100,
    //       color: '#E4D225'
    //     }
    //   );
    // } else {
    //   Gradient.push({
    //     offset: points[0] / 100,
    //     color: '#E01F28'
    //   });
    // }
    if (this.realValue / 2 > 50) {
      showValue = 100;
    } else if (this.realValue < 0) {
      showValue = 0;
    } else {
      showValue = this.realValue / 2 + 10;
    }
    // 因为柱状初始化为0，温度存在负值，所以加上负值60和空出距离10
    const option = {
      grid: {
        x: 35, //左侧与y轴的距离
        y: -145, //top部与x轴的距离
        x2: -200, //右侧与y轴的距离
        y2: 20 //bottom部与x轴的距离
      },
      // backgroundColor: '#0C2F6F',
      // backgroundColor: '#000',
      yAxis: [
        {
          show: false,
          data: [],
          min: 0,
          max: 120
        },
        {
          show: false,
          min: 0,
          max: 60
        }
      ],
      xAxis: [
        {
          show: false,
          min: -10,
          max: 80,
          data: []
        },
        {
          show: false,
          min: -10,
          max: 80,
          data: []
        },
        {
          show: false,
          min: -10,
          max: 80,
          data: []
        },
        {
          show: false,
          min: -5,
          max: 80
        }
      ],
      series: [
        {
          name: '条',
          type: 'bar',
          // 对应上面XAxis的第一个对象配置
          xAxisIndex: 0,
          data: [
            {
              value: showValue
            }
          ],
          barWidth: 8,
          itemStyle: {
            normal: {
              color: new echarts.graphic.LinearGradient(0, 1, 0, 0, Gradient)
            }
          },
          z: 2
        },
        {
          name: '白框',
          type: 'bar',
          xAxisIndex: 1,
          barGap: '-100%',
          data: [62],
          barWidth: 14,
          itemStyle: {
            normal: {
              color: '#0C2E6D',
              barBorderRadius: 50
            }
          },
          z: 1
        },
        {
          name: '外框',
          type: 'bar',
          xAxisIndex: 2,
          barGap: '-100%',
          data: [64],
          barWidth: 24,
          itemStyle: {
            normal: {
              color: '#4577BA',
              barBorderRadius: 50
            }
          },
          z: 0
        },
        {
          name: '圆',
          type: 'scatter',
          hoverAnimation: false,
          data: [0],
          xAxisIndex: 0,
          symbolSize: 20,
          itemStyle: {
            normal: {
              color: '#46a3ff',
              opacity: 1
            }
          },
          z: 2
        },
        {
          name: '白圆',
          type: 'scatter',
          hoverAnimation: false,
          data: [0],
          xAxisIndex: 1,
          symbolSize: 24,
          itemStyle: {
            normal: {
              color: '#0C2E6D',
              opacity: 1
            }
          },
          z: 1
        },
        {
          name: '外圆',
          type: 'scatter',
          hoverAnimation: false,
          data: [0],
          xAxisIndex: 2,
          symbolSize: 30,
          itemStyle: {
            normal: {
              color: '#4577BA',
              opacity: 1
            }
          },
          z: 0
        },
        {
          name: '刻度',
          type: 'bar',
          yAxisIndex: 0,
          xAxisIndex: 3,
          label: {
            normal: {
              show: true,
              position: 'left',
              distance: 10,
              color: '#666',
              fontSize: 14,
              // eslint-disable-next-line
              formatter: function(params: any) {
                if (params.dataIndex > 60 || params.dataIndex < 10) {
                  return '';
                } else {
                  if ((params.dataIndex - 10) % 10 === 0) {
                    return (params.dataIndex - 10) * 2;
                  } else {
                    return '';
                  }
                }
              }
            }
          },
          barGap: '-100%',
          data: scaleLineMarks,
          barWidth: 1,
          itemStyle: {
            normal: {
              color: '#666',
              barBorderRadius: 120
            }
          },
          z: 0
        }
      ]
    };
    // eslint-disable-next-line
    this.chartTarget!.setOption(option as any, true);
  }
}
