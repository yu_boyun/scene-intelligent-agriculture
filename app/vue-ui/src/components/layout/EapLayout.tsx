import { Component, Vue } from 'vue-property-decorator';
import style from '@/styles/components/layout.module.less';

@Component({})
export default class EapLayout extends Vue {
  public render() {
    return (
      <div class={style['eap-layout']}>
        {this.$slots.header && <div class={style['eap-layout-header']}>{this.$slots.header}</div>}
        <div class={style['eap-layout-body']}>{this.$slots.default}</div>
      </div>
    );
  }
}
