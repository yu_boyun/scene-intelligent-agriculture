import edgerosNetSvg from './edgeros-net.svg';
import waterOnSvg from './water-on.svg';
import waterOffSvg from './water-off.svg';
import humidifierOnSvg from './humidifier-on.svg';
import humidifierOffSvg from './humidifier-off.svg';

export {
  edgerosNetSvg,
  waterOnSvg,
  waterOffSvg,
  humidifierOnSvg,
  humidifierOffSvg
}