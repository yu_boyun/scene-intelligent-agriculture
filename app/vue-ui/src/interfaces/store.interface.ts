import { IUser } from './auth.interface';

export interface IStoreState {
  srand: string;
  token: string;
  isMobile: boolean; // 是否是移动端环境
  accountInfo: IUser; // 当前登录账号信息
}