export interface IPermission {
  ainn: boolean; // 是否允许 AI 神经网络计算
  alarm: boolean; // 是否有添加闹钟的权限
  share: boolean; // 此应用是否允许与其他应用共享信息
  notify: boolean; // 此应用是否允许推送消息
  advnwc: boolean; // 此应用是否允许高级网络控制
  network: boolean; // 此应用是否允许网络通信
  rtsp: boolean; // 此应用是否允许 RTSP 网络，例如：网络摄像头、网络麦克风。
  lora: boolean; // 此应用是否允许通过 LoRaWAN 网络发送或接收数据。
  coap: boolean; // 此应用是否允许 CoAP IoT 网络协议。
  wallpaper: boolean; // 此应用是否允许设置壁纸。
  //  MQTT 客户端子对象。
  mqtt: {
    publish: boolean; // 是否允许应用使用 MQTT 协议发布数据。
    subscribe: boolean; // 是否允许应用使用 MQTT 协议订阅消息
  };
  // 媒体中心访问权限
  mediacenter: {
    readable: boolean; // 可以读取媒体中心内容
    writable: boolean; // 可以编写媒体中心内容。
    removable: boolean; // 可以移除媒体中心内容。
  };
  // 手机功能。
  phone: {
    camera: boolean; // 手机摄像头。
    contacts: boolean; // 手机联系人。
    microphone: boolean; // 手机麦克风和 MIDI 输入。
    geolocation: boolean; // 手机地理位置。
  };
  devices: string[]; // 允许的设备 ID 列表。
}
