import { IResponse } from './common.interface';

export interface IDevice {
  devid: string; // 设备ID
  alias: string; // 设备别名
  report: {
    desc: string; // 设备描述信息
    excl: boolean; // 此设备是App专有的
    model: string; // 设备型号
    name: string; // 当前机器的名称
    type: string; // 机器的类型。通常为：'monitor'，'edger'，'device'
    vendor: string; // 设备制造商
  };
}

// Get device-list
export interface IDeviceListResp extends IResponse {
  data: IDevice[];
}

// Get member-list
export interface IMemberListResp extends IResponse {
  data: string[];
}

export interface IInitSceneResp extends IResponse {
  data: {
    devMap: Map<string, IDevice>;
    settings: { label: string; points: number[] };
  };
}
