import { Vue, Component } from "vue-property-decorator";
import "@/styles/eap.less";

@Component
export default class App extends Vue {
  public render() {
    return (
      <div id="app">
        {/* <transition name="start"> */}
          <router-view></router-view>
        {/* </transition> */}
      </div>
    );
  }
}
